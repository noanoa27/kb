<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\helpers\Html;
use yii\filters\AccessControl;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */

     
//אכיפת החוקים של ההרשאות
public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update'], //כל החוקים חלים רק על אפדייט
                'rules' => [
                    [
                        'allow' => true, 
                        'actions' => ['update'], 
                        'roles' => ['updateArticle'], //תאפשר לעשות אפדייט למי שיש את ההרשאה אפדייטארטיקל
                        'roleParams' => function() { //באיזה ארטיקל מדובר
                            return ['article' => Article::findOne(['id' => Yii::$app->request->get('id')])];
                        },
                    ],
                ],
            ],
        ];            
    }



    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
       //מוצאים את כל התגיות של המאמר ומשרשר אותם 
        $model = $this->findModel($id);
        $tagModels = $model->tags;

        $tags='';
        foreach($tagModels as $tag){
            $tagLink =Html::a($tag->name,['article/index','ArticleSearch[tag]'=>$tag->name]);//בונה קישור של html 
            $tags .=','.$tagLink; //שרשור
        }

        $tags=substr($tags,1); //מוריד מהראשון את הפסיק כי הראשון הוא ריק

        return $this->render('view', [
            'model' => $this->findModel($id),
            'tags'=>$tags
        ]); 
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) { //אם הגיעו נתונים בשיטת פוסט וגם הצלחנו לשמור את הנצונים
            return $this->redirect(['view', 'id' => $model->id]); //תציג את הויו של האיידי של מה שיצרנו עכשיו
        }

        return $this->render('create', [ //אם אחד מהתנאים לא התקיים אז נשארים באותו הדף
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
 // if (\Yii::$app->user->can('updateArticle',['article' => $model])) {
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
 // }
//  throw new ForbiddenHttpException('Sorry this is not your article');
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']); //מחזיר לדף הראשי
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
