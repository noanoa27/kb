<?php

namespace app\controllers;
use app\models\Tag; 
use yii\web\Response;
use Yii;

class TagController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionList($query)
    {
        $models = Tag::findAllByName($query); //רשימה של כל התגים שמתחילים בתו מסוים
        $items = [];
    
        foreach ($models as $model) {
            $items[] = ['name' => $model->name]; //הופכת את כל הרשימה למערך
        }
        // We know we can use ContentNegotiator filter
        // this way is easier to show you here :)
        Yii::$app->response->format = Response::FORMAT_JSON; //כל המערך הופך לפורמט של ג'ייסון
    
        return $items;
    }

}
